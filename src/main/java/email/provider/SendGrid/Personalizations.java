package email.provider.SendGrid;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Personalizations {

	@NotNull
	private List<To> to;
	
	@NotNull
	private String subject;

    @JsonProperty
	public String getSubject() {
		return subject;
	}

    @JsonProperty
	public void setSubject(String subject) {
		this.subject = subject;
	}

    @JsonProperty
	public List<To> getTo() {
		return to;
	}

    @JsonProperty
	public void setTo(List<To> to) {
		this.to = to;
	}
	
	
}
