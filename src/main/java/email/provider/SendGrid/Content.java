package email.provider.SendGrid;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Content {

	private String type = "text/plain";
	
	@NotNull
	private String value;

	@JsonProperty
	public String getType() {
		return type;
	}

	@JsonProperty
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty
	public String getValue() {
		return value;
	}

	@JsonProperty
	public void setValue(String value) {
		this.value = value;
	}
	
}
