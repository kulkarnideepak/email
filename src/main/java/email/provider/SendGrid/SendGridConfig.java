package email.provider.SendGrid;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SendGridConfig {
	
	@NotNull
	@Size(min=2, max=500)
	private String key = "Bearer fakekey";
	
	@NotNull
	@Size(min=2, max=500)
	private String url = "https://api.sendgrid.com/v3/mail/";

	@JsonProperty
	public String getKey() {
		return key;
	}

	@JsonProperty
	public void setKey(String key) {
		this.key = String.format("Bearer %s", key);
	}

	@JsonProperty
	public String getUrl() {
		return url;
	}
	
	@JsonProperty
	public void setUrl(String url) {
		this.url = url;
	}
}
