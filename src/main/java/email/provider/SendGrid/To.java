package email.provider.SendGrid;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class To {

	@NotNull
	private String email;

	@NotNull
	private String name;
	
	@JsonProperty
    public String getName() {
		return name;
	}

    @JsonProperty
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty
	public String getEmail() {
		return email;
	}

    @JsonProperty
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
