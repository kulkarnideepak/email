package email.provider.SendGrid;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageBody {

	@NotNull
	private List<Personalizations> personalizations;
	
	@NotNull
	private From from;
	
	@NotNull
	private List<Content> content;

	@JsonProperty
	public List<Personalizations> getPersonalizations() {
		return personalizations;
	}

	@JsonProperty
	public void setPersonalizations(List<Personalizations> personalizations) {
		this.personalizations = personalizations;
	}

	@JsonProperty
	public From getFrom() {
		return from;
	}

	@JsonProperty
	public void setFrom(From from) {
		this.from = from;
	}

	@JsonProperty
	public List<Content> getContent() {
		return content;
	}

	@JsonProperty
	public void setContent(List<Content> content) {
		this.content = content;
	}
}
