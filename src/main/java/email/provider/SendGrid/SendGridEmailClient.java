package email.provider.SendGrid;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import email.EmailConfiguration;
import email.api.EmailTemplate;
import email.client.GenericEmailClient;

public class SendGridEmailClient extends GenericEmailClient {

	Logger logger = Logger.getLogger(SendGridEmailClient.class);

	public SendGridEmailClient(EmailConfiguration configuration, Client client) {
		this.client = client;
		this.apiKey = configuration.getSendGridConfig().getKey();
		this.weburi = configuration.getSendGridConfig().getUrl();
		logger.info(weburi);
		logger.info(apiKey);

	}

	private MessageBody buildMessageBody(EmailTemplate emailTemplate) {
		MessageBody messageBody = new MessageBody();

		To to = new To();
		to.setEmail(emailTemplate.getTo());
		to.setName(emailTemplate.getToName());
		List<To> toList = new ArrayList<To>();
		toList.add(to);

		Personalizations personalizations = new Personalizations();
		personalizations.setTo(toList);
		personalizations.setSubject(emailTemplate.getSubject());

		List<Personalizations> personalizationsList = new ArrayList<Personalizations>();
		personalizationsList.add(personalizations);

		From from = new From();
		from.setEmail(emailTemplate.getFrom());
		from.setName(emailTemplate.getFromName());

		Content content = new Content();
		content.setValue(emailTemplate.getBody());
		List<Content> contentList = new ArrayList<Content>();
		contentList.add(content);

		messageBody.setPersonalizations(personalizationsList);
		messageBody.setFrom(from);
		messageBody.setContent(contentList);

		return messageBody;
	}

	@Override
	public Response sendEmail(EmailTemplate emailTemplate) throws Exception{
		Response response = client.target(this.weburi)
				.path("send")
				.request(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE)
				.header("Authorization", this.apiKey)
				.post(Entity.entity(buildMessageBody(emailTemplate), MediaType.APPLICATION_JSON));

		logger.info("Response :" + response);
		return response;
	}
}
