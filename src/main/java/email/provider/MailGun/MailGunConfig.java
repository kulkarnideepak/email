package email.provider.MailGun;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MailGunConfig {
	
	@NotNull
	@Size(min=2, max=5)
	private String user = "api";
	
	@NotNull
	@Size(min=2, max=50)
	private String key = "fakekey";
	
	@NotNull
	@Size(min=2, max=500)
	private String url = "https://api.mailgun.net/v3/sandbox7ab4a320ac0948c69229205d84d85dad.mailgun.org/";

	@JsonProperty
	public String getUser() {
		return user;
	}

	@JsonProperty
	public void setUser(String user) {
		this.user = user;
	}

	@JsonProperty
	public String getKey() {
		return key;
	}

	@JsonProperty
	public void setKey(String key) {
		this.key = key;
	}

	@JsonProperty
	public String getUrl() {
		return url;
	}
	
	@JsonProperty
	public void setUrl(String url) {
		this.url = url;
	}
}
