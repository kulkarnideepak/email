package email.provider.MailGun;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import email.EmailConfiguration;
import email.api.EmailTemplate;
import email.client.GenericEmailClient;

public class MailGunEmailClient extends GenericEmailClient {

	Logger logger = Logger.getLogger(MailGunEmailClient.class);

	private final String user;
	private final String apiKey;

	public MailGunEmailClient(EmailConfiguration configuration, Client client) {
		this.client = client;
		this.user = configuration.getMailGunConfig().getUser();
		this.apiKey = configuration.getMailGunConfig().getKey();
		this.weburi = configuration.getMailGunConfig().getUrl();
		
		logger.info(weburi);
		logger.info(apiKey);

	}

	@Override
	public Response sendEmail(EmailTemplate emailTemplate) throws Exception{
		MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();

		formData.add("subject", emailTemplate.getSubject());
		formData.add("text", emailTemplate.getBody());
		formData.add("from", String.format("%s <%s>", emailTemplate.getFromName(), emailTemplate.getFrom()));
		formData.add("to", String.format("%s <%s>", emailTemplate.getToName(), emailTemplate.getTo()));

		String authorizationHeaderValue = "Basic "
				+ java.util.Base64.getEncoder().encodeToString(String.format("%s:%s", user, apiKey).getBytes());

		Response response = client
				.target(weburi)
				.path("message")
				.request(MediaType.APPLICATION_FORM_URLENCODED)
				.header("Authorization", authorizationHeaderValue)
				.post(Entity.form(formData));

		logger.info("Response :" + response);
		
		return response;
	}
}
