package email;

import javax.ws.rs.client.Client;

import email.resources.EmailResource;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class EmailApplication extends Application<EmailConfiguration> {

	public static void main(final String[] args) throws Exception {
		new EmailApplication().run(args);
	}

	@Override
	public String getName() {
		return "email";
	}

	@Override
	public void initialize(final Bootstrap<EmailConfiguration> bootstrap) {
	}

	@Override
	public void run(final EmailConfiguration configuration, final Environment environment) {
		
		//-- Define Email resource and register it with Jersey service
		final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
				.using(environment).build("email");
		final EmailResource resource = new EmailResource(
				configuration, client);
		environment.jersey().register(resource);
	}

}
