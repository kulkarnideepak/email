package email.resources;

import java.util.concurrent.atomic.AtomicLong;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Response;

import com.codahale.metrics.annotation.Timed;

import email.EmailConfiguration;
import email.api.EmailTemplate;
import email.api.ResourceResponse;
import email.client.GenericEmailClient;
import email.provider.MailGun.MailGunEmailClient;
import email.provider.SendGrid.SendGridEmailClient;

@Path("/email")
@Produces(MediaType.APPLICATION_JSON)
public class EmailResource {
	Logger logger = Logger.getLogger(EmailResource.class);

	private final AtomicLong counter;
	private final String emailProvider;
	private final GenericEmailClient emailClient;

	public EmailResource(EmailConfiguration configuration, Client client) {
		this.counter = new AtomicLong();
		this.emailProvider = configuration.getEmailProvider();
		emailClient = this.getEmailClientByProviderName(emailProvider, configuration, client);
	}

	@POST
	@Timed
	/**
	 * Send email using specified emailProvider. Information about email is provided in emailTemplate
	 * @param emailTemplate
	 * @return
	 * @throws Exception
	 */
	public ResourceResponse sendEmail(@NotNull @Valid EmailTemplate emailTemplate) throws Exception {
		logger.info(emailTemplate);
		emailClient.sendEmail(emailTemplate);
		return new ResourceResponse(counter.incrementAndGet(), "email sent");
	}

	/*
	 * Bare bone implementation. Could have tried to use dependency injection so that
	 * based on value of emailProvider property, this email client automatically gets injected
	 */
	private GenericEmailClient getEmailClientByProviderName(String emailProvider, EmailConfiguration configuration,
			Client client) {
		GenericEmailClient emailClient = null;
		logger.info(emailProvider);
		switch (emailProvider) {
		case "MailGun":
			emailClient = new MailGunEmailClient(configuration, client);
			break;
		case "SendGrid":
		default:
			emailClient = new SendGridEmailClient(configuration, client);
			break;
		}

		return emailClient;
	}
}
