package email;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import email.provider.MailGun.MailGunConfig;
import email.provider.SendGrid.SendGridConfig;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

public class EmailConfiguration extends Configuration {
	@NotEmpty
	private String emailProvider;
	
	@NotEmpty
	private String sendGridKey;
	
	@NotEmpty
	private String mailGunKey;
	
	@Valid
	@NotNull
	@JsonProperty
	private MailGunConfig mailGunConfig = new MailGunConfig();

	@Valid
	@NotNull
	@JsonProperty
	private SendGridConfig sendGridConfig = new SendGridConfig();
	
	public SendGridConfig getSendGridConfig() {
		sendGridConfig.setKey(this.getSendGridKey());
		return sendGridConfig;
	}

	public void setSendGridConfig(SendGridConfig sendGridConfig) {
		this.sendGridConfig = sendGridConfig;
	}

	@JsonProperty
	public MailGunConfig getMailGunConfig() {
		mailGunConfig.setKey(this.getMailGunKey());
		return mailGunConfig;
	}

	@JsonProperty
	public void setMailGunConfig(MailGunConfig mailGunConfig) {
		this.mailGunConfig = mailGunConfig;
	}

	public String getSendGridKey() {
		return sendGridKey;
	}

	public void setSendGridKey(String sendGridKey) {
		this.sendGridKey = sendGridKey;
	}

	public String getMailGunKey() {
		return mailGunKey;
	}

	public void setMailGunKey(String mailGunKey) {
		this.mailGunKey = mailGunKey;
	}

	@JsonProperty
	public String getEmailProvider() {
		return emailProvider;
	}

	@JsonProperty
	public void setEmailProvider(String emailProvider) {
		this.emailProvider = emailProvider;
	}

	@Valid
	@NotNull
	@JsonProperty
	private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

	public JerseyClientConfiguration getJerseyClientConfiguration() {
		return httpClient;
	}
}
