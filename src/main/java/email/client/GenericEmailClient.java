package email.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import email.api.EmailTemplate;

public abstract class GenericEmailClient {

	protected Client client;
	protected String weburi;
	protected String apiKey;

	
	public abstract Response sendEmail(EmailTemplate emailTemplate) throws Exception;
}
