package email.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

public class ResourceResponse {
    private long id;

    @Length(max = 3)
    private String content;

    public ResourceResponse() {
        // Jackson deserialization
    }

    public ResourceResponse(long id, String content) {
        this.id = id;
        this.content = content;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }
}