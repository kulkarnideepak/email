package email.api;

import javax.validation.constraints.NotNull;

import org.jsoup.Jsoup;

public class EmailTemplate {

	@NotNull
	private String to;
	
	@NotNull
	private String toName;
	
	@NotNull
	private String from;
	
	@NotNull
	private String fromName;
	
	@NotNull
	private String subject;
	
	@NotNull
	private String body;
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		//-- Remove HTNL tags from text 
		this.body = Jsoup.parse(body).text();;
	}
	@Override
	public String toString() {
		return "EmailTemplate [to=" + to + ", toName=" + toName + ", from=" + from + ", fromName=" + fromName
				+ ", subject=" + subject + ", body=" + body + "]";
	}
	
	
	
}
