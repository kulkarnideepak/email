# Email

How to get application from bitbucket
--------------------------------------------
Code for this application can be downloaded using git clone from 
[Git Repository](https://bitbucket.org/kulkarnideepak/email)

How to start the email application
--------------------------------------------

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/email-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter [url](http://localhost:8080/email)

How to invoke REST API to send email
-------------------------------------------
- API URL : `http://localhost:8080/email`
- Payload : 
```
{
	"to":"example@gmail.com",
	"toName":"Example Sender",
	"from":"postmaster@sandbox7ab4a320ac0948c69229205d84d85dad.mailgun.org",
	"fromName":"Mailgun Sandbox",
	"subject":"Test mail",
	"body":"This is a test mail"
}
```
- Response: `200 OK`

Changing email provider
-------------------------------------------
Value of email provided can be changed by changing value of `emailProvider` property in `config.yml` file. It takes following two values for emailProvider:
`SendGrid`
`MailGun`
NOTE: `Server has to be restarted for a property change to take effect`


Technology and Architecture used for this application
-------------------------------------------------------

This application is a jetty based service deployed using dropwizard. `www.dropwizard.io`. Java was used as a language of choice. Dropwizard was a choice of implementation due to ease of implementation it provides for developing microservices. Also has very good support for collecting metrics and monitoring services in terms of healthchecks, performances etc. In addition to using default libraries from dropwizard, I also included dropwizard-client library to make outgoing http calls to external services such as SendGrid and MailGun. Jsoup library was used to remove HTML tags from email body. Jsoup could also have been used to further enhance the text.

Tradeoffs and other choices
---------------------------------

Requirement in this particular exercise could have been served out of a simple JavaScript base service as well. e.g. use of AngularJS based service or react framework would have been good enough. However, due to lack of time and lack of extensive working knowledge with these services, I had to stick with traditional Java approach.

Things that can be improved
------------------------------

1. URL addresses are persisted in the Configuration class itself. Ideally they should be picked up from a `yml` file so that it is easy to configure them and also easier to manage changes in values.
2. Lack of healthcheck mechanism.
3. Missing test cases. Typlically, I do not write code with out any test cases. But for this exercise, I did not have time to write them. 
4. Better exception handling. Again due to lack of time, I could not do exception handling.
5. Better logging could have been used
6. Changing of service provider should technically have zero downtime and should be done via a REST API. or it can even be automated with a few of following scenarios:
  a. If one service goes down, then email service can after 3-5 failed attempts, automatically switches to alternate option
  b. We can use rule based choice of selecting email service provider. e.g. 60% of mails get sent using SendGrid and 40% mails gets sent using MailGun
  
